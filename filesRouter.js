const express = require('express');
const router = express.Router();
const { createFile, getFiles, getFile, deleteFile, editFile } = require('./filesService.js');
const validateBodyFileData = require('./middleware/validateBodyFileData');
const isFileExists = require('./middleware/isFileExists');
const validateBodyEditEndpoint = require('./middleware/validateBodyEditEndpoint');
const isValidFileExtension = require('./middleware/isValidFileExtension');

router.post('/', isValidFileExtension, validateBodyFileData, createFile);

router.get('/', getFiles);

router.get('/:filename', isFileExists, getFile);

// Other endpoints - put, delete.
router.put('/:filename', isFileExists, validateBodyEditEndpoint, editFile);

router.delete('/:filename', isFileExists, deleteFile);

module.exports = {
  filesRouter: router
};
