const fs = require('fs');
const express = require('express');
const morgan = require('morgan')
const app = express();

const { filesRouter } = require('./filesRouter.js');
const path = require('path');

app.use(express.json());
app.use(morgan('tiny', { stream: fs.createWriteStream('./requests.log', { flags: 'a' }) }));

app.use('/api/files', filesRouter);

const start = async () => {
  try {
    if (!fs.existsSync('files')) {
      fs.mkdirSync('files');
    }
    global.FILES_PATH = path.join(path.resolve(), 'files');
    app.listen(8080);
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
}

start();

//ERROR HANDLER
app.use(errorHandler)

function errorHandler (err, req, res, next) {
  console.error(err)
  res.status(500).send({'message': 'Server error'});
}
