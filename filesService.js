// requires...
const fs = require('fs');
const path = require('path');
const getPathToFile = require('./utils/getPathToFile');

function createFile (req, res, next) {
  fs.writeFile(getPathToFile(req.body.filename), req.body.content, (err) => {
    console.error(err);
    if (err) {
      res.status(400).send({'message' : 'Failed to save the file'});
    } else {
      res.status(200).send({ 'message': 'File created successfully' });
    }
  });
}

async function getFiles (req, res, next) {
  let files
  try {
    files = await new Promise((resolve, reject) => {
      fs.readdir(FILES_PATH, (err, files) => {
        if (err) {
          console.error(err);
          reject(err);
        }
        resolve(files);
      });
    });

    if (files === undefined) files = [];
    res.status(200).send({
      'message': 'Success',
      'files': files
    });
  } catch (err) {
    console.error(err);
    res.status(500).send({ 'message': 'Server error' });
  }
}

const getFile = async (req, res, next) => {
  const pathToFile = getPathToFile(req.params.filename);
  const parsedPath = path.parse(pathToFile);
  let fileStats;

  // Retrieve file stats;
  try {
    fileStats = await new Promise((resolve, reject) => {
      fs.stat(pathToFile, (err, stats) => {
        if (err) reject();
        resolve(stats);
      });
    });
  } catch {
    res.status(500).send({ 'message': 'Server error' });
  }

  // Retrieve file content
  try {
    const content = await new Promise((resolve, reject) => {
      fs.readFile(pathToFile, 'utf-8', (err, content) => {
        if (err) reject(err);
        resolve(content);
      });
    });

    const [filename, extension, uploadedDate] = [parsedPath.base, path.extname(pathToFile).slice(1), fileStats.ctime];

    res.status(200).send({
      'message': 'Success',
      'filename': filename,
      'content': content,
      'extension': extension,
      'uploadedDate': uploadedDate
    });
  } catch (err) {
    console.error(err);
    res.status(500).send({ 'message': 'Server error' });
  }
}

// Other functions - editFile, deleteFile
function editFile(req, res, next) {
  console.log(req.body.content);
  fs.writeFile(getPathToFile(req.params.filename), req.body.content, { flag: 'a'}, (err) => {
    console.log(err);
    if (err) {
      res.status(500).send({'message' : 'Failed to edit the file'});
    } else {
      res.status(200).send({ 'message': 'File edited successfully' });
    }
  });
}

function deleteFile(req, res, next) {
  const pathToFile = getPathToFile(req.params.filename);

  fs.unlinkSync(pathToFile);

  res.status(200).send({ 'message': 'Success' });
}

module.exports = {
  createFile,
  getFiles,
  getFile,
  editFile,
  deleteFile
}
