const path = require('path');

function getPathToFile(filename) {
    return path.join(FILES_PATH, filename);
}

module.exports = getPathToFile;