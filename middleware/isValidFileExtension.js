function isValidFileExtension(req, res, next) {
    if (/\.(log|txt|json|yaml|xml|js)$/.test(req.body.filename)) { // check if valid
        next();
    } else {
        res.status(400).send({ 'message': 'This file extension is not supported' });
    }
}

module.exports = isValidFileExtension;