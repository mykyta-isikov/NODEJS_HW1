function validateBodyEditEndpoint(req, res, next) {
    if (typeof req.body.content === 'string') {
        next();
    } else {
        res.status(400).send({'message' : 'Content should be a string'});

    }
}

module.exports = validateBodyEditEndpoint;