function validateBodyFileData(req, res, next) {
    if (typeof req.body.filename === 'string' && typeof req.body.content === 'string') {
        next();
    } else {
        res.status(400).send({'message': 'Filename and content should be string'});

    }
}

module.exports = validateBodyFileData;