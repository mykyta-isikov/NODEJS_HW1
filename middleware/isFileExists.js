const fs = require('fs');
const getPathToFile = require('../utils/getPathToFile');

async function validateBodyFileData(req, res, next) {
    const pathToFile = getPathToFile(req.params.filename);
    const fileExists = fs.existsSync(pathToFile);
    if (fileExists) {
        next();
    } else {
        res.status(400).send({'message' : 'File does not exist'});
    }

}

module.exports = validateBodyFileData;